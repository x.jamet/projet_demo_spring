package payroll.status;

public enum Status {

	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}