package payroll.controller;


import java.util.List;
import java.util.stream.Collectors;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import payroll.assembler.OrderResourceAssembler;
import payroll.entities.Order;

import payroll.exception.OrderNotFoundException;
import payroll.repositories.OrderRepository;
import payroll.status.Status;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


@RestController
public
class OrderController {

	private final OrderRepository orderRepository;
	private final OrderResourceAssembler assembler;

	OrderController(OrderRepository orderRepository,
					OrderResourceAssembler assembler) {

		this.orderRepository = orderRepository;
		this.assembler = assembler;
	}

	@RequestMapping(value="/orders", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public Resources<Resource<Order>>  all() {

    	List<Resource<Order>> orders = orderRepository.findAll().stream()
        		.map(order -> new Resource<>(order,
        			linkTo(methodOn(OrderController.class).one(order.getId())).withSelfRel(),
        			linkTo(methodOn(OrderController.class).all()).withRel("order")))
        		.collect(Collectors.toList());
    	
    	return new Resources<>(orders,
        		linkTo(methodOn(OrderController.class).all()).withSelfRel());
	}
	@RequestMapping(value="/orders/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public Resource<Order> one(@PathVariable Long id) {

		Order order =  orderRepository.findById(id)
				.orElseThrow(() -> new OrderNotFoundException(id));
		return assembler.toResource(order);
	}



	@RequestMapping(value="/orders", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	ResponseEntity<Resource<Order>> newOrder(@RequestBody Order order) {

		order.setStatus(Status.IN_PROGRESS);
		Order newOrder = orderRepository.save(order);

		return ResponseEntity
			.created(linkTo(methodOn(OrderController.class).one(newOrder.getId())).toUri())
			.body(assembler.toResource(newOrder));
	}
	
	@RequestMapping(value="/orders/{id}/cancel", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
	ResponseEntity<ResourceSupport> cancel(@PathVariable Long id) {

		Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

		if (order.getStatus() == Status.IN_PROGRESS) {
			order.setStatus(Status.CANCELLED);
			return ResponseEntity.ok(assembler.toResource(orderRepository.save(order)));
		}

		return ResponseEntity
			.status(HttpStatus.METHOD_NOT_ALLOWED)
			.body(new VndErrors.VndError("Method not allowed", "You can't cancel an order that is in the " + order.getStatus() + " status"));
	}
	
	
	@RequestMapping(value="/orders/{id}/complete", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
	ResponseEntity<ResourceSupport> complete(@PathVariable Long id) {

			Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

			if (order.getStatus() == Status.IN_PROGRESS) {
				order.setStatus(Status.COMPLETED);
				return ResponseEntity.ok(assembler.toResource(orderRepository.save(order)));
			}

			return ResponseEntity
				.status(HttpStatus.METHOD_NOT_ALLOWED)
				.body(new VndErrors.VndError("Method not allowed", "You can't complete an order that is in the " + order.getStatus() + " status"));
	}
}