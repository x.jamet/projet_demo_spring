package payroll.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;


import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import payroll.assembler.EmployeeResourceAssembler;
import payroll.entities.Employee;
import payroll.exception.EmployeeNotFoundException;
import payroll.repositories.EmployeeRepository;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


@RestController
public
class EmployeeController {

	private final EmployeeRepository repository;
	private EmployeeResourceAssembler assembler;

	
	EmployeeController(EmployeeRepository repository, EmployeeResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	// Aggregate root




	// Single item
	
	
	
	@RequestMapping(value="/employees/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
    Resource<Employee> one(@PathVariable Long id) {

    	Employee employee = repository.findById(id)
            .orElseThrow(() -> new EmployeeNotFoundException(id));

        return new Resource<>(employee,
            linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel(),
            linkTo(methodOn(EmployeeController.class).all()).withRel("users"));
    }
	
    @RequestMapping(value="/employees", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
    Resources<Resource<Employee>> all() {

    	List<Resource<Employee>> employees = repository.findAll().stream()
    		.map(employee -> new Resource<>(employee,
    			linkTo(methodOn(EmployeeController.class).one(employee.getId())).withSelfRel(),
    			linkTo(methodOn(EmployeeController.class).all()).withRel("employees")))
    		.collect(Collectors.toList());

    	return new Resources<>(employees,
    		linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
    }
    
    
    @RequestMapping(value="/employees", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> newEmployee(@RequestBody Employee newEmployee) throws URISyntaxException {

    	Resource<Employee> resource = assembler.toResource(repository.save(newEmployee));

    	return ResponseEntity
    		.created(new URI(resource.getId().expand().getHref()))
    		.body(resource);
    	
    }
    @RequestMapping(value="/employees/{id}", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) throws URISyntaxException {

    	Employee updatedEmployee = repository.findById(id)
    		.map(employee -> {
    			employee.setName(newEmployee.getName());
    			employee.setRole(newEmployee.getRole());
    			return repository.save(employee);
    		})
    		.orElseGet(() -> {
    			newEmployee.setId(id);
    			return repository.save(newEmployee);
    		});

    	Resource<Employee> resource = assembler.toResource(updatedEmployee);

    	return ResponseEntity
    		.created(new URI(resource.getId().expand().getHref()))
    		.body(resource);
    }
    
    @RequestMapping(value="/employees/{id}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> deleteEmployee(@PathVariable Long id) {

    	repository.deleteById(id);

    	return ResponseEntity.noContent().build();
    }
    
	@RequestMapping(value="/form", method = RequestMethod.GET)
	public ModelAndView form(ModelAndView mav) {
		mav.setViewName("formAddEmployee");
		mav.addObject("employee", new Employee());
		return mav;
	}
    /*
	@PostMapping("/employees")
	Employee newEmployee(@RequestBody Employee newEmployee) {
		return repository.save(newEmployee);
	}
	*/

    /*
	@PutMapping("/employees/{id}")
	Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {

		return repository.findById(id)
			.map(employee -> {
				employee.setName(newEmployee.getName());
				employee.setRole(newEmployee.getRole());
				return repository.save(employee);
			})
			.orElseGet(() -> {
				newEmployee.setId(id);
				return repository.save(newEmployee);
			});
	}
     */
    
    /*
	@DeleteMapping("/employees/{id}")
	void deleteEmployee(@PathVariable Long id) {
		repository.deleteById(id);
	}
	*/
}