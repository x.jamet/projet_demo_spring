package payroll.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;


import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import payroll.assembler.EntrepriseResourceAssembler;
import payroll.entities.Entreprise;
import payroll.exception.EntrepriseNotFoundException;
import payroll.repositories.EntrepriseRepository;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


@RestController
public
class EntrepriseController {

	private final EntrepriseRepository repository;
	private EntrepriseResourceAssembler assembler;

	
	EntrepriseController(EntrepriseRepository repository, EntrepriseResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	// Aggregate root




	// Single item
	
	
	
	@RequestMapping(value="/entreprises/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
    Resource<Entreprise> one(@PathVariable Long id) {

		Entreprise entreprise = repository.findById(id)
            .orElseThrow(() -> new EntrepriseNotFoundException(id));

        return new Resource<>(entreprise,
            linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel(),
            linkTo(methodOn(EmployeeController.class).all()).withRel("users"));
    }
	
    @RequestMapping(value="/entreprises", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
	public
    Resources<Resource<Entreprise>> all() {

    	List<Resource<Entreprise>> entreprises = repository.findAll().stream()
    		.map(entreprise -> new Resource<>(entreprise,
    			linkTo(methodOn(EntrepriseController.class).one(entreprise.getId())).withSelfRel(),
    			linkTo(methodOn(EntrepriseController.class).all()).withRel("entreprises")))
    		.collect(Collectors.toList());

    	return new Resources<>(entreprises,
    		linkTo(methodOn(EntrepriseController.class).all()).withSelfRel());
    }
    
    
    @RequestMapping(value="/entreprises", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> newEmployee(@RequestBody Entreprise newEntreprise) throws URISyntaxException {

    	Resource<Entreprise> resource = assembler.toResource(repository.save(newEntreprise));

    	return ResponseEntity
    		.created(new URI(resource.getId().expand().getHref()))
    		.body(resource);
    	
    }
    @RequestMapping(value="/entreprises/{id}", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> replaceEntreprise(@RequestBody Entreprise newEntreprise, @PathVariable Long id) throws URISyntaxException {

    	Entreprise updatedEntreprise = repository.findById(id)
    		.map(entreprise -> {
    			entreprise.setName(newEntreprise.getName());
    			entreprise.setDescription(newEntreprise.getDescription());
    			return repository.save(entreprise);
    		})
    		.orElseGet(() -> {
    			newEntreprise.setId(id);
    			return repository.save(newEntreprise);
    		});

    	Resource<Entreprise> resource = assembler.toResource(updatedEntreprise);

    	return ResponseEntity
    		.created(new URI(resource.getId().expand().getHref()))
    		.body(resource);
    }
    
    @RequestMapping(value="/entreprises/{id}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    ResponseEntity<?> deleteEntreprise(@PathVariable Long id) {

    	repository.deleteById(id);

    	return ResponseEntity.noContent().build();
    }
    
}