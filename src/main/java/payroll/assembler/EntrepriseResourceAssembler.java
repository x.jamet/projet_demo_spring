package payroll.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import payroll.controller.EntrepriseController;
import payroll.entities.Entreprise;

@Component
public
class EntrepriseResourceAssembler implements ResourceAssembler<Entreprise, Resource<Entreprise>> {

	@Override
	public Resource<Entreprise> toResource(Entreprise entreprise) {

		return new Resource<>(entreprise,
			linkTo(methodOn(EntrepriseController.class).one(entreprise.getId())).withSelfRel(),
			linkTo(methodOn(EntrepriseController.class).all()).withRel("entreprises"));
	}
}