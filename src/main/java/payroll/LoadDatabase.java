package payroll;

import lombok.extern.slf4j.Slf4j;


import payroll.entities.*;

import payroll.repositories.*;

import payroll.status.Status;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

	@Bean
	CommandLineRunner initDatabase(EmployeeRepository repository, OrderRepository orderRepository, EntrepriseRepository entrepriseRepository) {
		return args -> {
			

			log.info("Preloading " +orderRepository.save(new Order("MacBook Pro", Status.COMPLETED)));
			log.info("Preloading " +orderRepository.save(new Order("iPhone", Status.IN_PROGRESS)));
			
			
			Entreprise entreprise1 = new Entreprise("Apple", "Marque de smartPhone");
			log.info("Preloading "+ entrepriseRepository.save(entreprise1));
			
			Entreprise entreprise2 = new Entreprise("Macdo", "FastFood");
			log.info("Preloading "+ entrepriseRepository.save(entreprise2));

			Employee employe1 = new Employee("Bilbo", "Baggins", "burglar", 10);
			log.info("Preloading " + repository.save(employe1));
			
			Employee employe2 = new Employee("Frodo", "Baggins", "thief", 12);
			log.info("Preloading " + repository.save(employe2));
			
			
			
	};
}
}