package payroll.entities;


import lombok.Data;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Data
@Entity
public
class Entreprise {

	private @Id @GeneratedValue Long id;
	private String Name;
	private String description;
	


	public Entreprise(){
		
	}
	
	public Entreprise(String name, String description) {
		this.Name = name;
		this.description = description;

	}
	
}