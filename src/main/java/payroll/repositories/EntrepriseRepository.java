package payroll.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import payroll.entities.Entreprise;

public interface EntrepriseRepository extends JpaRepository<Entreprise, Long> {

}