package payroll.exception;


public class EmployeeNotFoundException extends RuntimeException{

	public EmployeeNotFoundException(Long id) {
		// TODO Auto-generated constructor stub
		super("Could not find employee " + id);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



}
