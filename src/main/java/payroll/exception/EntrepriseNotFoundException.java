package payroll.exception;

public class EntrepriseNotFoundException extends RuntimeException{

	public EntrepriseNotFoundException(Long id) {
		// TODO Auto-generated constructor stub
		super("Could not find entreprise " + id);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
